
#include <sstream>
#include <fstream>
#include <vector>
#include <memory>
#include <iostream>

#include <SFML/Network.hpp>
#include <RF24.h>

std::vector<std::pair<std::string, std::string>> logins;
const int PORT = 44551;
// const std::vector<std::pair<std::string, std::string>> logins = { {"brenton", "12345"}, {"adam", "bennett"} };
// Setup for GPIO 22 CE and CE0 CSN with SPI Speed @ 4Mhz
RF24 radio(RPI_V2_GPIO_P1_15, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_4MHZ);
// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };


struct Client
{
public:
	sf::TcpSocket socket;
	std::string name;
};

typedef std::shared_ptr<Client> ClientPtr;

void acceptNewClients(std::vector<ClientPtr>& clients, sf::TcpListener& listener, sf::SocketSelector& selector)
{
	ClientPtr newClient = std::make_shared<Client>();
	if(listener.accept(newClient->socket) != sf::Socket::Done)	
		return;
	sf::Packet loginPacket;
	if(newClient->socket.receive(loginPacket) != sf::Socket::Done)
		return;

	// TODO: Make sure packet is in correct format
	std::string username, password;
	loginPacket >> username >> password;

	bool loginSuccessful = false;
	for(auto login : logins)
	{
		if(login.first == username && login.second == password)
			loginSuccessful = true;
	}

	// Make sure name is not already in use
	for(auto c : clients)
	{
		if(c->name == username)
			loginSuccessful = false;
	}

	newClient->name = username;

	sf::Packet loginResponse;
	loginResponse << loginSuccessful;

	newClient->socket.send(loginResponse);

	if(loginSuccessful)
	{	
		clients.push_back(newClient);
		selector.add(newClient->socket);
		std::cout << "New Client!" << std::endl;
	}
}

std::vector<std::pair<ClientPtr, sf::Packet>> receiveData(std::vector<ClientPtr>& clients, sf::SocketSelector& selector)
{
	std::vector<std::pair<ClientPtr, sf::Packet>> data;
	std::vector<std::vector<ClientPtr>::iterator> removeClients;

	for(auto itr = clients.begin(); itr != clients.end(); itr++)
	{
		auto c = *itr;
		if(selector.isReady(c->socket))
		{
			sf::Packet packet;
			auto status = c->socket.receive(packet);
			if(status == sf::Socket::Status::Done)
			{
				data.push_back({c, packet});
			}
			else
				removeClients.push_back(itr);
		}
	}

	for(auto itr : removeClients)
	{
		std::cout << "Disconnected to: " << (*itr)->name << std::endl;
		selector.remove( (*itr)->socket );
		clients.erase(itr);
	}

	return data;
}

std::vector<std::pair<std::string, std::string>> getLoginInfo(const std::string& filename)
{
	std::ifstream file(filename);
	std::vector<std::pair<std::string, std::string>> logins;
	if(!file.is_open())
	{
		std::cout << "Failed to open file!" << std::endl;
		return logins;
	}

	std::string str;
	while(std::getline(file, str))
	{
		std::istringstream stream(str);

		std::pair<std::string, std::string> login;
		stream >> login.first >> login.second;
		logins.push_back(login);
	}

	return logins;
}

int main()
{	
   const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

	radio.begin();
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(1, pipes[0]);

    radio.setPALevel(RF24_PA_MAX);
    radio.powerUp() ;

    radio.setChannel(12);
//      radio.setRetries(0, 15);
    radio.stopListening();
    radio.printDetails();



//        logins = getLoginInfo("logins.txt");
	logins = { {"1", "brenton" }, {"2", "james"}, {"3", "nick"} };

	for(auto login : logins)
		std::cout << login.first << ", " << login.second << std::endl;

	std::vector<ClientPtr> clients;
	sf::SocketSelector selector;
	sf::TcpListener listener;
	listener.setBlocking(false);

	if(listener.listen(PORT) != sf::Socket::Done)
	{
		std::cout << "Failed to bind listener..." << std::endl;
		return -1;
	}

	std::cout << "Binded to port " << PORT << "!" << std::endl;

	sf::Clock clock;
	const sf::Time updatePeriod = sf::milliseconds(50);
	bool run = true;
	while(run)
	{
		sf::sleep(updatePeriod - clock.getElapsedTime());

		// Update at 10Hz
		if(clock.getElapsedTime() >= updatePeriod)
		{

			// Accept any new clients
			acceptNewClients(clients, listener, selector);
			std::vector<std::pair<ClientPtr, sf::Packet>> data;

			// Receive data from clients
			if(selector.wait(sf::seconds(5)))
				data = receiveData(clients, selector);

			// Process data
			for(auto pair : data)
			{
				int data = -1;
				pair.second >> data;

				std::cout << "[" << pair.first->name << "]: ";
				std::cout << data << std::endl;

				// Send data to client
				int command = 1000 * std::stoi(pair.first->name) + data;
				std::cout << radio.write(&command, sizeof(command)) << std::endl;
			}


			clock.restart();
		}
	}

	return 0;
}
