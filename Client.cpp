#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <string>

const int PORT = 44551;

int getCommand()
{
	int msg = -1;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		msg = 1;
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		msg = 2;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		msg = 3;
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		msg = 4;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		msg = 0;

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		msg = 5;
	else if(sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		msg = 6;

	return msg;
}

int main()
{
	// Connect to the server
	std::string ip;
	std::cout << "IP: ";
	std::cin >> ip;

	sf::TcpSocket socket;
	auto status = socket.connect(ip, PORT); 
	if(status != sf::Socket::Done)
	{
		std::cout << "Failed to connect to server..." << std::endl;
		return -1;
	}

	// Login to the server
	std::string username, password;
	std::cout << "Username: ";
	std::cin >> username;

	std::cout << "Password: ";
	std::cin >> password;

	sf::Packet loginPacket;
	loginPacket << username << password;
	if(socket.send(loginPacket) != sf::Socket::Done)
	{
		std::cout << "Failed to login..." << std::endl;
		return -1;
	}

	// Check if the user is logged in
	sf::Packet loginStatus;
	if(socket.receive(loginStatus) != sf::Socket::Done)
	{
		std::cout << "Invalid username/password!" << std::endl;
		return -1;
	}

	bool isLoginSuccessful;
	loginStatus >> isLoginSuccessful;
	if(!isLoginSuccessful)
	{
		std::cout << "Invalid username/password!" << std::endl;
		return -1;
	}
	else
		std::cout << "Logged into server!!!" << std::endl;




	sf::Clock clock;
	const sf::Time updatePeriod = sf::milliseconds(100);

	bool run = true;
	while(run)
	{
		sf::sleep(updatePeriod - clock.getElapsedTime());

		// Update at 10Hz
		if(clock.getElapsedTime() >= updatePeriod)
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				run = false;

			int msg = getCommand();

			if(msg != -1)
			{
				sf::Packet packet;
				packet << msg;
				auto status = socket.send(packet);
				if(status == sf::Socket::Done)
					std::cout<< "Sent: " << msg << std::endl;
				else
					std::cout << "Failed to send!!!" << std::endl;
			}
		
			clock.restart();
		}
	}

	return 0;
}
